(error "query failed: rule validation failed when checking: (exists ((x!1 Int)
         (x!2 Int)
         (x!3 Int)
         (x!4 Real)
         (x!5 Bool)
         (x!6 Real)
         (x!7 Real)
         (x!8 Int)
         (x!9 Int)
         (x!10 Int)
         (x!11 Int)
         (x!12 Int)
         (x!13 Int)
         (x!14 Int)
         (x!15 Int)
         (x!16 Int)
         (x!17 Int)
         (x!18 Int)
         (x!19 Int)
         (x!20 Int)
         (x!21 Real)
         (x!22 Real)
         (x!23 Real)
         (x!24 Real)
         (x!25 Real)
         (x!26 Bool)
         (x!27 Bool)
         (x!28 Bool)
         (x!29 Int)
         (x!30 Bool)
         (x!31 Bool)
         (x!32 Int)
         (x!33 Int)
         (x!34 Bool)
         (x!35 Bool)
         (x!36 Int)
         (x!37 Int)
         (x!38 Real)
         (x!39 Int)
         (x!40 Real)
         (x!41 Bool)
         (x!42 Real)
         (x!43 Real)
         (x!44 Bool)
         (x!45 Bool)
         (x!46 Bool)
         (x!47 Real)
         (x!48 Bool)
         (x!49 Bool)
         (x!50 Real)
         (x!51 Bool)
         (x!52 Bool)
         (x!53 Bool))
  (let ((a!1 (or (<= (- 10.0) (+ x!38 (* (- 1.0) x!42))) (= x!23 (- 10.0))))
        (a!2 (not (<= (+ x!38 (* (- 1.0) x!42)) 10.0)))
        (a!4 (or (<= (+ x!38 (* (- 1.0) x!42)) 10.0) (= x!23 10.0)))
        (a!5 (not (<= (- 10.0) (+ x!38 (* (- 1.0) x!42)))))
        (a!6 (and (or (not (= x!39 3)) (= x!43 0.0))
                  (or (= x!39 3) (= x!43 0.0))))
        (a!9 (and (or (not (<= x!4 100.0)) (= x!24 x!4))
                  (or (<= x!4 100.0) (= x!24 100.0))))
        (a!12 (or (and (or x!51 (= x!36 0)) (or (not x!51) (= x!36 1)))
                  (>= 0 x!16)))
        (a!14 (and (or (not (<= x!40 100.0)) (= x!25 x!40))
                   (or (<= x!40 100.0) (= x!25 100.0))))
        (a!17 (or (and (or x!52 (= x!37 0)) (or (not x!52) (= x!37 1)))
                  (>= 0 x!12)))
        (a!19 (and (or (not (= x!32 0)) (= x!38 x!43))
                   (or (= x!32 0) (= x!38 x!42))))
        (a!21 (and (or (not x!51) (= x!18 (+ 1 x!1))) (or x!51 (= x!18 0))))
        (a!22 (or (and (or x!51 (= x!17 0)) (or (not x!51) (= x!17 1)))
                  (>= 0 x!16)))
        (a!23 (or (and (or x!52 (= x!13 0)) (or (not x!52) (= x!13 1)))
                  (>= 0 x!12)))
        (a!24 (and (or (not x!52) (= x!14 (+ 1 x!2))) (or x!52 (= x!14 0))))
        (a!25 (or (and x!46 (not (= x!39 6))) (= x!47 (/ 1.0 20.0))))
        (a!26 (or (and x!49 (not (= x!39 5))) (= x!50 (- (/ 1.0 20.0)))))
        (a!27 (= x!40 (to_real (div (to_int x!23) 20)))))
  (let ((a!3 (or a!2 (= x!23 (+ x!38 (* (- 1.0) x!42)))))
        (a!7 (and (or a!6 (= x!39 4)) (or (not (= x!39 4)) (= x!43 0.0))))
        (a!10 (and (or a!9 (not (<= 0.0 x!4))) (or (<= 0.0 x!4) (= x!24 0.0))))
        (a!13 (and a!12 (or (not (>= 0 x!16)) (= x!36 0))))
        (a!15 (and (or a!14 (not (<= 0.0 x!40)))
                   (or (<= 0.0 x!40) (= x!25 0.0))))
        (a!18 (and a!17 (or (not (>= 0 x!12)) (= x!37 0))))
        (a!20 (and (or a!19 (not (<= x!22 100.0)))
                   (or (<= x!22 100.0) (= x!38 100.0)))))
  (let ((a!8 (and (or a!7 (= x!39 5)) (or (not (= x!39 5)) (= x!43 x!50))))
        (a!11 (or a!10 (and (not (= x!3 6)) (not (= x!3 4)) (not (= x!3 5)))))
        (a!16 (or a!15 (and (not (= x!39 6)) (not (= x!39 4)) (not (= x!39 5))))))
    (and (not false)
         (= x!41 (not (= x!32 0)))
         (= x!44 (= x!39 4))
         (= x!45 (= x!39 6))
         (= x!48 (= x!39 5))
         (or (not x!30)
             x!27
             x!28
             (not (>= x!42 15.0))
             (= x!9 1)
             (not (= x!29 3)))
         (or (= x!3 6) (= x!3 4) (= x!3 5) (= x!24 0.0))
         (or (= x!39 6) (= x!39 4) (= x!39 5) (= x!25 0.0))
         (or (not x!46) (= x!39 6) (= x!47 0.0))
         (or (not x!49) (= x!39 5) (= x!50 0.0))
         (or (= x!10 1) (not (= x!9 1)))
         (or (= x!10 0) (= x!9 1))
         (or (= x!32 0) (= x!22 x!42))
         (or (not (= x!32 0)) (= x!22 x!43))
         (or (not (= x!36 20)) (= x!19 1))
         (or (= x!36 20) (= x!19 0))
         (or (not (= x!37 20)) (= x!20 1))
         (or (= x!37 20) (= x!20 0))
         (or (not (= x!39 6)) (= x!43 x!47))
         (or (not (>= 0 x!12)) (= x!13 0))
         (or (not (>= 0 x!16)) (= x!17 0))
         (or (not (>= 0 x!11)) (= x!14 0))
         (or (not (>= 0 x!15)) (= x!18 0))
         (or (<= x!13 20) (= x!37 20))
         (or (<= x!17 20) (= x!36 20))
         a!1
         (or (<= 0.0 x!22) (= x!38 0.0))
         (or (and x!30 (not x!27) (not x!28) (>= x!42 15.0) (= x!29 3))
             (= x!9 0))
         (or (and a!3 a!4) a!5)
         (or a!8 (= x!39 6))
         a!11
         (or a!13 (not (<= x!17 20)))
         a!16
         (or a!18 (not (<= x!13 20)))
         (or a!20 (not (<= 0.0 x!22)))
         (or a!21 (>= 0 x!15))
         a!22
         a!23
         (or a!24 (>= 0 x!11))
         a!25
         a!26
         (or (not x!5) (= x!21 x!6))
         (or x!5 (= x!21 x!7))
         (or (not x!26) (= x!8 1))
         (or x!26 (= x!8 0))
         (or x!35 (= x!32 0))
         (or (not x!35) (= x!32 0))
         (or (not x!35) (= x!33 1))
         (or x!35 (= x!33 0))
         (or (not x!35) (= x!39 1))
         (or x!35 (= x!39 0))
         (or (not x!51) (= x!16 1))
         (or x!51 (= x!16 0))
         (or x!51 (= x!15 0))
         (or (not x!51) (= x!15 (+ 1 x!1)))
         (or (not x!52) (= x!12 1))
         (or x!52 (= x!12 0))
         (or x!52 (= x!11 0))
         (or (not x!52) (= x!11 (+ 1 x!2)))
         (= x!31 true)
         (= x!34 true)
         (= x!35 true)
         (= x!46 true)
         (= x!49 true)
         (not x!53)
         a!27)))))")
unknown
(:SPACER-expand-node-undef             19
 :SPACER-max-depth                     3
 :SPACER-max-query-lvl                 3
 :SPACER-num-invariants                22
 :SPACER-num-lemmas                    27
 :SPACER-num-properties                2
 :SPACER-num-queries                   24
 :added-eqs                            26471
 :arith-add-rows                       2604
 :arith-assert-diseq                   8711
 :arith-assert-lower                   11727
 :arith-assert-upper                   9384
 :arith-bound-prop                     629
 :arith-conflicts                      28
 :arith-eq-adapter                     4491
 :arith-fixed-eqs                      1922
 :arith-gcd-tests                      30
 :arith-ineq-splits                    5
 :arith-offset-eqs                     1151
 :arith-pivots                         603
 :bool-inductive-gen                   22
 :bool-inductive-gen-failures          26
 :conflicts                            372
 :decisions                            18687
 :del-clause                           3012
 :final-checks                         71
 :interface-eqs                        3
 :max-memory                           13.54
 :memory                               12.03
 :minimized-lits                       126
 :mk-bool-var                          6621
 :mk-clause                            8405
 :num-allocs                           30116009
 :num-checks                           151
 :propagations                         132842
 :rlimit-count                         498134
 :time                                 0.35
 :time.itp_solver.itp_core             0.02
 :time.spacer.init_rules               0.00
 :time.spacer.init_rules.pt.init       0.00
 :time.spacer.solve                    0.31
 :time.spacer.solve.propagate          0.04
 :time.spacer.solve.reach              0.27
 :time.spacer.solve.reach.children     0.05
 :time.spacer.solve.reach.gen.bool_ind 0.10
 :time.virtual_solver.proof            0.01
 :time.virtual_solver.smt.total        0.24
 :time.virtual_solver.smt.total.sat    0.05
 :time.virtual_solver.smt.total.undef  0.13
 :virtual_solver.checks                151
 :virtual_solver.checks.sat            14
 :virtual_solver.checks.undef          46)
(:max-memory   13.54
 :memory       12.03
 :mk-bool-var  1
 :num-allocs   30116009
 :rlimit-count 498134
 :time         0.35
 :total-time   0.36)

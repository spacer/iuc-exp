(error "query failed: rule validation failed when checking: (exists ((x!1 Int)
         (x!2 Int)
         (x!3 Int)
         (x!4 Real)
         (x!5 Bool)
         (x!6 Real)
         (x!7 Real)
         (x!8 Int)
         (x!9 Int)
         (x!10 Int)
         (x!11 Int)
         (x!12 Int)
         (x!13 Int)
         (x!14 Int)
         (x!15 Int)
         (x!16 Int)
         (x!17 Int)
         (x!18 Int)
         (x!19 Int)
         (x!20 Int)
         (x!21 Real)
         (x!22 Real)
         (x!23 Real)
         (x!24 Real)
         (x!25 Real)
         (x!26 Real)
         (x!27 Bool)
         (x!28 Bool)
         (x!29 Bool)
         (x!30 Int)
         (x!31 Bool)
         (x!32 Bool)
         (x!33 Int)
         (x!34 Int)
         (x!35 Bool)
         (x!36 Bool)
         (x!37 Int)
         (x!38 Int)
         (x!39 Int)
         (x!40 Real)
         (x!41 Bool)
         (x!42 Real)
         (x!43 Real)
         (x!44 Bool)
         (x!45 Bool)
         (x!46 Real)
         (x!47 Bool)
         (x!48 Bool)
         (x!49 Real)
         (x!50 Bool)
         (x!51 Bool)
         (x!52 Bool))
  (let ((a!1 (or (<= (- 10.0) (+ x!23 (* (- 1.0) x!42))) (= x!24 (- 10.0))))
        (a!2 (not (<= (+ x!23 (* (- 1.0) x!42)) 10.0)))
        (a!4 (or (<= (+ x!23 (* (- 1.0) x!42)) 10.0) (= x!24 10.0)))
        (a!5 (not (<= (- 10.0) (+ x!23 (* (- 1.0) x!42)))))
        (a!6 (and (or (not (= x!39 3)) (= x!43 0.0))
                  (or (= x!39 3) (= x!43 0.0))))
        (a!9 (and (or (not (= x!33 0)) (= x!23 x!43))
                  (or (= x!33 0) (= x!23 x!42))))
        (a!11 (and (or (not (<= x!4 100.0)) (= x!25 x!4))
                   (or (<= x!4 100.0) (= x!25 100.0))))
        (a!14 (or (and (or x!50 (= x!37 0)) (or (not x!50) (= x!37 1)))
                  (>= 0 x!16)))
        (a!16 (and (or (not (<= x!40 100.0)) (= x!26 x!40))
                   (or (<= x!40 100.0) (= x!26 100.0))))
        (a!19 (or (and (or x!51 (= x!38 0)) (or (not x!51) (= x!38 1)))
                  (>= 0 x!12)))
        (a!21 (or (and (or x!50 (= x!17 0)) (or (not x!50) (= x!17 1)))
                  (>= 0 x!16)))
        (a!22 (and (or (not x!50) (= x!18 (+ 1 x!1))) (or x!50 (= x!18 0))))
        (a!23 (or (and (or x!51 (= x!13 0)) (or (not x!51) (= x!13 1)))
                  (>= 0 x!12)))
        (a!24 (and (or (not x!51) (= x!14 (+ 1 x!2))) (or x!51 (= x!14 0))))
        (a!25 (or (and x!45 (not (= x!39 6))) (= x!46 (/ 1.0 20.0))))
        (a!26 (or (and x!48 (not (= x!39 5))) (= x!49 (- (/ 1.0 20.0)))))
        (a!27 (= x!40 (to_real (div (to_int x!24) 20)))))
  (let ((a!3 (or a!2 (= x!24 (+ x!23 (* (- 1.0) x!42)))))
        (a!7 (and (or a!6 (= x!39 4)) (or (not (= x!39 4)) (= x!43 0.0))))
        (a!10 (and (or a!9 (not (<= x!22 100.0)))
                   (or (<= x!22 100.0) (= x!23 100.0))))
        (a!12 (and (or a!11 (not (<= 0.0 x!4))) (or (<= 0.0 x!4) (= x!25 0.0))))
        (a!15 (and a!14 (or (not (>= 0 x!16)) (= x!37 0))))
        (a!17 (and (or a!16 (not (<= 0.0 x!40)))
                   (or (<= 0.0 x!40) (= x!26 0.0))))
        (a!20 (and a!19 (or (not (>= 0 x!12)) (= x!38 0)))))
  (let ((a!8 (and (or a!7 (= x!39 5)) (or (not (= x!39 5)) (= x!43 x!49))))
        (a!13 (or a!12 (and (not (= x!3 6)) (not (= x!3 4)) (not (= x!3 5)))))
        (a!18 (or a!17 (and (not (= x!39 6)) (not (= x!39 4)) (not (= x!39 5))))))
    (and (not false)
         (= x!41 (not (= x!33 0)))
         (= x!44 (= x!39 6))
         (= x!47 (= x!39 5))
         (or (not x!31)
             x!28
             x!29
             (not (>= x!42 15.0))
             (= x!9 1)
             (not (= x!30 3)))
         (or (= x!3 6) (= x!3 4) (= x!3 5) (= x!25 0.0))
         (or (= x!39 6) (= x!39 4) (= x!39 5) (= x!26 0.0))
         (or (not x!45) (= x!39 6) (= x!46 0.0))
         (or (not x!48) (= x!39 5) (= x!49 0.0))
         (or (not (= x!9 1)) (= x!10 1))
         (or (= x!9 1) (= x!10 0))
         (or (= x!33 0) (= x!22 x!42))
         (or (not (= x!33 0)) (= x!22 x!43))
         (or (not (= x!37 20)) (= x!19 1))
         (or (= x!37 20) (= x!19 0))
         (or (not (= x!38 20)) (= x!20 1))
         (or (= x!38 20) (= x!20 0))
         (or (not (= x!39 6)) (= x!43 x!46))
         (or (not (>= 0 x!15)) (= x!18 0))
         (or (not (>= 0 x!11)) (= x!14 0))
         (or (not (>= 0 x!16)) (= x!17 0))
         (or (not (>= 0 x!12)) (= x!13 0))
         (or (<= x!13 20) (= x!38 20))
         (or (<= x!17 20) (= x!37 20))
         a!1
         (or (<= 0.0 x!22) (= x!23 0.0))
         (or (and x!31 (not x!28) (not x!29) (>= x!42 15.0) (= x!30 3))
             (= x!9 0))
         (or (and a!3 a!4) a!5)
         (or a!8 (= x!39 6))
         (or a!10 (not (<= 0.0 x!22)))
         a!13
         (or a!15 (not (<= x!17 20)))
         a!18
         (or a!20 (not (<= x!13 20)))
         a!21
         (or a!22 (>= 0 x!15))
         a!23
         (or a!24 (>= 0 x!11))
         a!25
         a!26
         (or (not x!5) (= x!21 x!6))
         (or x!5 (= x!21 x!7))
         (or (not x!27) (= x!8 1))
         (or x!27 (= x!8 0))
         (or x!36 (= x!33 0))
         (or (not x!36) (= x!33 0))
         (or (not x!36) (= x!34 1))
         (or x!36 (= x!34 0))
         (or (not x!36) (= x!39 1))
         (or x!36 (= x!39 0))
         (or x!50 (= x!15 0))
         (or (not x!50) (= x!15 (+ 1 x!1)))
         (or (not x!50) (= x!16 1))
         (or x!50 (= x!16 0))
         (or x!51 (= x!11 0))
         (or (not x!51) (= x!11 (+ 1 x!2)))
         (or (not x!51) (= x!12 1))
         (or x!51 (= x!12 0))
         (= x!32 true)
         (= x!35 true)
         (= x!36 true)
         (= x!45 true)
         (= x!48 true)
         (not x!52)
         a!27)))))")
unknown
(:SPACER-expand-node-undef             4
 :SPACER-inductive-level               1
 :SPACER-max-depth                     3
 :SPACER-max-query-lvl                 3
 :SPACER-num-invariants                7
 :SPACER-num-lemmas                    16
 :SPACER-num-properties                7
 :SPACER-num-queries                   13
 :added-eqs                            11412
 :arith-add-rows                       980
 :arith-assert-diseq                   3590
 :arith-assert-lower                   4425
 :arith-assert-upper                   3932
 :arith-bound-prop                     406
 :arith-conflicts                      15
 :arith-eq-adapter                     1951
 :arith-fixed-eqs                      730
 :arith-gcd-tests                      7
 :arith-offset-eqs                     492
 :arith-pivots                         321
 :bool-inductive-gen                   11
 :bool-inductive-gen-failures          10
 :conflicts                            197
 :decisions                            8456
 :del-clause                           2304
 :final-checks                         29
 :max-memory                           12.67
 :memory                               11.21
 :minimized-lits                       24
 :mk-bool-var                          4523
 :mk-clause                            7057
 :num-allocs                           15219908
 :num-checks                           70
 :propagations                         57217
 :rlimit-count                         273658
 :time                                 0.20
 :time.itp_solver.itp_core             0.01
 :time.spacer.init_rules               0.00
 :time.spacer.init_rules.pt.init       0.00
 :time.spacer.solve                    0.16
 :time.spacer.solve.propagate          0.03
 :time.spacer.solve.reach              0.12
 :time.spacer.solve.reach.children     0.02
 :time.spacer.solve.reach.gen.bool_ind 0.03
 :time.virtual_solver.proof            0.00
 :time.virtual_solver.smt.total        0.12
 :time.virtual_solver.smt.total.sat    0.05
 :time.virtual_solver.smt.total.undef  0.03
 :virtual_solver.checks                70
 :virtual_solver.checks.sat            19
 :virtual_solver.checks.undef          10)
(:max-memory   12.67
 :memory       11.21
 :mk-bool-var  1
 :num-allocs   15219908
 :rlimit-count 273658
 :time         0.20
 :total-time   0.21)

(error "query failed: rule validation failed when checking: (exists ((x!1 Int)
         (x!2 Int)
         (x!3 Int)
         (x!4 Real)
         (x!5 Bool)
         (x!6 Real)
         (x!7 Real)
         (x!8 Int)
         (x!9 Int)
         (x!10 Int)
         (x!11 Int)
         (x!12 Int)
         (x!13 Int)
         (x!14 Int)
         (x!15 Int)
         (x!16 Int)
         (x!17 Int)
         (x!18 Int)
         (x!19 Int)
         (x!20 Int)
         (x!21 Real)
         (x!22 Real)
         (x!23 Real)
         (x!24 Real)
         (x!25 Real)
         (x!26 Bool)
         (x!27 Bool)
         (x!28 Bool)
         (x!29 Int)
         (x!30 Bool)
         (x!31 Bool)
         (x!32 Int)
         (x!33 Int)
         (x!34 Bool)
         (x!35 Bool)
         (x!36 Int)
         (x!37 Int)
         (x!38 Int)
         (x!39 Real)
         (x!40 Real)
         (x!41 Bool)
         (x!42 Real)
         (x!43 Real)
         (x!44 Bool)
         (x!45 Bool)
         (x!46 Real)
         (x!47 Bool)
         (x!48 Bool)
         (x!49 Real)
         (x!50 Bool)
         (x!51 Bool)
         (x!52 Bool))
  (let ((a!1 (= x!31 (or (not (= x!38 4)) (= x!39 0.0))))
        (a!2 (or (<= (- 10.0) (+ x!23 (* (- 1.0) x!42))) (= x!24 (- 10.0))))
        (a!3 (not (<= (+ x!23 (* (- 1.0) x!42)) 10.0)))
        (a!5 (or (<= (+ x!23 (* (- 1.0) x!42)) 10.0) (= x!24 10.0)))
        (a!6 (not (<= (- 10.0) (+ x!23 (* (- 1.0) x!42)))))
        (a!7 (and (or (not (= x!38 3)) (= x!43 0.0))
                  (or (= x!38 3) (= x!43 0.0))))
        (a!10 (and (or (not (= x!32 0)) (= x!23 x!43))
                   (or (= x!32 0) (= x!23 x!42))))
        (a!12 (and (or (not (<= x!4 100.0)) (= x!25 x!4))
                   (or (<= x!4 100.0) (= x!25 100.0))))
        (a!15 (or (and (or x!50 (= x!36 0)) (or (not x!50) (= x!36 1)))
                  (>= 0 x!16)))
        (a!17 (and (or (not (<= x!40 100.0)) (= x!39 x!40))
                   (or (<= x!40 100.0) (= x!39 100.0))))
        (a!20 (or (and (or x!51 (= x!37 0)) (or (not x!51) (= x!37 1)))
                  (>= 0 x!12)))
        (a!22 (and (or (not x!50) (= x!18 (+ 1 x!1))) (or x!50 (= x!18 0))))
        (a!23 (or (and (or x!50 (= x!17 0)) (or (not x!50) (= x!17 1)))
                  (>= 0 x!16)))
        (a!24 (or (and (or x!51 (= x!13 0)) (or (not x!51) (= x!13 1)))
                  (>= 0 x!12)))
        (a!25 (and (or (not x!51) (= x!14 (+ 1 x!2))) (or x!51 (= x!14 0))))
        (a!26 (or (and x!45 (not (= x!38 6))) (= x!46 (/ 1.0 20.0))))
        (a!27 (or (and x!48 (not (= x!38 5))) (= x!49 (- (/ 1.0 20.0)))))
        (a!28 (= x!40 (to_real (div (to_int x!24) 20)))))
  (let ((a!4 (or a!3 (= x!24 (+ x!23 (* (- 1.0) x!42)))))
        (a!8 (and (or a!7 (= x!38 4)) (or (not (= x!38 4)) (= x!43 0.0))))
        (a!11 (and (or a!10 (not (<= x!22 100.0)))
                   (or (<= x!22 100.0) (= x!23 100.0))))
        (a!13 (and (or a!12 (not (<= 0.0 x!4))) (or (<= 0.0 x!4) (= x!25 0.0))))
        (a!16 (and a!15 (or (not (>= 0 x!16)) (= x!36 0))))
        (a!18 (and (or a!17 (not (<= 0.0 x!40)))
                   (or (<= 0.0 x!40) (= x!39 0.0))))
        (a!21 (and a!20 (or (not (>= 0 x!12)) (= x!37 0)))))
  (let ((a!9 (and (or a!8 (= x!38 5)) (or (not (= x!38 5)) (= x!43 x!49))))
        (a!14 (or a!13 (and (not (= x!3 6)) (not (= x!3 4)) (not (= x!3 5)))))
        (a!19 (or a!18 (and (not (= x!38 6)) (not (= x!38 4)) (not (= x!38 5))))))
    (and (not false)
         a!1
         (= x!41 (not (= x!32 0)))
         (= x!44 (= x!38 6))
         (= x!47 (= x!38 5))
         (or (not x!30)
             x!27
             x!28
             (not (>= x!42 15.0))
             (= x!9 1)
             (not (= x!29 3)))
         (or (= x!3 6) (= x!3 4) (= x!3 5) (= x!25 0.0))
         (or (= x!38 6) (= x!38 4) (= x!38 5) (= x!39 0.0))
         (or (not x!45) (= x!38 6) (= x!46 0.0))
         (or (not x!48) (= x!38 5) (= x!49 0.0))
         (or (= x!10 1) (not (= x!9 1)))
         (or (= x!10 0) (= x!9 1))
         (or (= x!32 0) (= x!22 x!42))
         (or (not (= x!32 0)) (= x!22 x!43))
         (or (not (= x!36 20)) (= x!19 1))
         (or (= x!36 20) (= x!19 0))
         (or (not (= x!37 20)) (= x!20 1))
         (or (= x!37 20) (= x!20 0))
         (or (not (= x!38 6)) (= x!43 x!46))
         (or (not (>= 0 x!11)) (= x!14 0))
         (or (not (>= 0 x!12)) (= x!13 0))
         (or (not (>= 0 x!15)) (= x!18 0))
         (or (not (>= 0 x!16)) (= x!17 0))
         (or (<= x!13 20) (= x!37 20))
         (or (<= x!17 20) (= x!36 20))
         a!2
         (or (<= 0.0 x!22) (= x!23 0.0))
         (or (and x!30 (not x!27) (not x!28) (>= x!42 15.0) (= x!29 3))
             (= x!9 0))
         (or (and a!4 a!5) a!6)
         (or a!9 (= x!38 6))
         (or a!11 (not (<= 0.0 x!22)))
         a!14
         (or a!16 (not (<= x!17 20)))
         a!19
         (or a!21 (not (<= x!13 20)))
         (or a!22 (>= 0 x!15))
         a!23
         a!24
         (or a!25 (>= 0 x!11))
         a!26
         a!27
         (or (not x!5) (= x!21 x!6))
         (or x!5 (= x!21 x!7))
         (or (not x!26) (= x!8 1))
         (or x!26 (= x!8 0))
         (or x!35 (= x!32 0))
         (or (not x!35) (= x!32 0))
         (or (not x!35) (= x!33 1))
         (or x!35 (= x!33 0))
         (or (not x!35) (= x!38 1))
         (or x!35 (= x!38 0))
         (or x!50 (= x!15 0))
         (or (not x!50) (= x!15 (+ 1 x!1)))
         (or (not x!50) (= x!16 1))
         (or x!50 (= x!16 0))
         (or x!51 (= x!11 0))
         (or (not x!51) (= x!11 (+ 1 x!2)))
         (or (not x!51) (= x!12 1))
         (or x!51 (= x!12 0))
         (= x!34 true)
         (= x!35 true)
         (= x!45 true)
         (= x!48 true)
         (not x!52)
         a!28)))))")
unknown
(:SPACER-expand-node-undef             3
 :SPACER-max-depth                     2
 :SPACER-max-query-lvl                 2
 :SPACER-num-invariants                5
 :SPACER-num-lemmas                    7
 :SPACER-num-properties                2
 :SPACER-num-queries                   5
 :added-eqs                            4022
 :arith-add-rows                       517
 :arith-assert-diseq                   844
 :arith-assert-lower                   1376
 :arith-assert-upper                   1153
 :arith-bound-prop                     209
 :arith-conflicts                      9
 :arith-eq-adapter                     639
 :arith-fixed-eqs                      178
 :arith-gcd-tests                      4
 :arith-ineq-splits                    1
 :arith-offset-eqs                     86
 :arith-pivots                         162
 :bool-inductive-gen                   3
 :bool-inductive-gen-failures          3
 :conflicts                            98
 :decisions                            2553
 :del-clause                           825
 :final-checks                         9
 :interface-eqs                        1
 :max-memory                           12.12
 :memory                               10.72
 :minimized-lits                       20
 :mk-bool-var                          2892
 :mk-clause                            5177
 :num-allocs                           9820080
 :num-checks                           23
 :propagations                         15974
 :rlimit-count                         160770
 :time                                 0.10
 :time.itp_solver.itp_core             0.00
 :time.spacer.init_rules               0.00
 :time.spacer.init_rules.pt.init       0.00
 :time.spacer.solve                    0.06
 :time.spacer.solve.propagate          0.02
 :time.spacer.solve.reach              0.04
 :time.spacer.solve.reach.children     0.01
 :time.spacer.solve.reach.gen.bool_ind 0.00
 :time.virtual_solver.proof            0.00
 :time.virtual_solver.smt.total        0.05
 :time.virtual_solver.smt.total.sat    0.02
 :time.virtual_solver.smt.total.undef  0.01
 :virtual_solver.checks                23
 :virtual_solver.checks.sat            4
 :virtual_solver.checks.undef          3)
(:max-memory   12.12
 :memory       10.72
 :mk-bool-var  1
 :num-allocs   9820080
 :rlimit-count 160770
 :time         0.10
 :total-time   0.11)

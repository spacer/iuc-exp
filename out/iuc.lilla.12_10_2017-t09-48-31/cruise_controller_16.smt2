(error "query failed: rule validation failed when checking: (exists ((x!1 Int)
         (x!2 Int)
         (x!3 Int)
         (x!4 Real)
         (x!5 Bool)
         (x!6 Real)
         (x!7 Real)
         (x!8 Int)
         (x!9 Int)
         (x!10 Int)
         (x!11 Int)
         (x!12 Int)
         (x!13 Int)
         (x!14 Int)
         (x!15 Int)
         (x!16 Int)
         (x!17 Int)
         (x!18 Int)
         (x!19 Int)
         (x!20 Int)
         (x!21 Real)
         (x!22 Real)
         (x!23 Real)
         (x!24 Real)
         (x!25 Real)
         (x!26 Real)
         (x!27 Bool)
         (x!28 Bool)
         (x!29 Bool)
         (x!30 Int)
         (x!31 Bool)
         (x!32 Bool)
         (x!33 Int)
         (x!34 Int)
         (x!35 Bool)
         (x!36 Bool)
         (x!37 Int)
         (x!38 Int)
         (x!39 Int)
         (x!40 Real)
         (x!41 Bool)
         (x!42 Real)
         (x!43 Real)
         (x!44 Bool)
         (x!45 Bool)
         (x!46 Real)
         (x!47 Bool)
         (x!48 Bool)
         (x!49 Real)
         (x!50 Bool)
         (x!51 Bool)
         (x!52 Bool))
  (let ((a!1 (or (and (not (= x!39 3))
                      (not (= x!39 6))
                      (not (= x!39 4))
                      (not (= x!39 5)))
                 (>= x!26 15.0)))
        (a!2 (or (<= (- 10.0) (+ x!26 (* (- 1.0) x!42))) (= x!23 (- 10.0))))
        (a!3 (not (<= (+ x!26 (* (- 1.0) x!42)) 10.0)))
        (a!5 (or (<= (+ x!26 (* (- 1.0) x!42)) 10.0) (= x!23 10.0)))
        (a!6 (not (<= (- 10.0) (+ x!26 (* (- 1.0) x!42)))))
        (a!7 (and (or (not (= x!39 3)) (= x!43 0.0))
                  (or (= x!39 3) (= x!43 0.0))))
        (a!10 (and (or (not (<= x!4 100.0)) (= x!24 x!4))
                   (or (<= x!4 100.0) (= x!24 100.0))))
        (a!13 (or (and (or x!50 (= x!37 0)) (or (not x!50) (= x!37 1)))
                  (>= 0 x!16)))
        (a!15 (and (or (not (<= x!40 100.0)) (= x!25 x!40))
                   (or (<= x!40 100.0) (= x!25 100.0))))
        (a!18 (and (or (not (= x!33 0)) (= x!26 x!43))
                   (or (= x!33 0) (= x!26 x!42))))
        (a!20 (or (and (or x!51 (= x!38 0)) (or (not x!51) (= x!38 1)))
                  (>= 0 x!12)))
        (a!22 (or (and (or x!50 (= x!17 0)) (or (not x!50) (= x!17 1)))
                  (>= 0 x!16)))
        (a!23 (and (or (not x!50) (= x!18 (+ 1 x!1))) (or x!50 (= x!18 0))))
        (a!24 (or (and (or x!51 (= x!13 0)) (or (not x!51) (= x!13 1)))
                  (>= 0 x!12)))
        (a!25 (and (or (not x!51) (= x!14 (+ 1 x!2))) (or x!51 (= x!14 0))))
        (a!26 (or (and x!45 (not (= x!39 6))) (= x!46 (/ 1.0 20.0))))
        (a!27 (or (and x!48 (not (= x!39 5))) (= x!49 (- (/ 1.0 20.0)))))
        (a!28 (= x!40 (to_real (div (to_int x!23) 20)))))
  (let ((a!4 (or a!3 (= x!23 (+ x!26 (* (- 1.0) x!42)))))
        (a!8 (and (or a!7 (= x!39 4)) (or (not (= x!39 4)) (= x!43 0.0))))
        (a!11 (and (or a!10 (not (<= 0.0 x!4))) (or (<= 0.0 x!4) (= x!24 0.0))))
        (a!14 (and a!13 (or (not (>= 0 x!16)) (= x!37 0))))
        (a!16 (and (or a!15 (not (<= 0.0 x!40)))
                   (or (<= 0.0 x!40) (= x!25 0.0))))
        (a!19 (and (or a!18 (not (<= x!22 100.0)))
                   (or (<= x!22 100.0) (= x!26 100.0))))
        (a!21 (and a!20 (or (not (>= 0 x!12)) (= x!38 0)))))
  (let ((a!9 (and (or a!8 (= x!39 5)) (or (not (= x!39 5)) (= x!43 x!49))))
        (a!12 (or a!11 (and (not (= x!3 6)) (not (= x!3 4)) (not (= x!3 5)))))
        (a!17 (or a!16 (and (not (= x!39 6)) (not (= x!39 4)) (not (= x!39 5))))))
    (and (not false)
         (= x!32 a!1)
         (= x!41 (not (= x!33 0)))
         (= x!44 (= x!39 6))
         (= x!47 (= x!39 5))
         (or (not x!31)
             x!28
             x!29
             (not (>= x!42 15.0))
             (= x!9 1)
             (not (= x!30 3)))
         (or (= x!3 6) (= x!3 4) (= x!3 5) (= x!24 0.0))
         (or (= x!39 6) (= x!39 4) (= x!39 5) (= x!25 0.0))
         (or (not x!45) (= x!39 6) (= x!46 0.0))
         (or (not x!48) (= x!39 5) (= x!49 0.0))
         (or (not (= x!9 1)) (= x!10 1))
         (or (= x!9 1) (= x!10 0))
         (or (= x!33 0) (= x!22 x!42))
         (or (not (= x!33 0)) (= x!22 x!43))
         (or (not (= x!37 20)) (= x!19 1))
         (or (= x!37 20) (= x!19 0))
         (or (not (= x!38 20)) (= x!20 1))
         (or (= x!38 20) (= x!20 0))
         (or (not (= x!39 6)) (= x!43 x!46))
         (or (not (>= 0 x!11)) (= x!14 0))
         (or (not (>= 0 x!15)) (= x!18 0))
         (or (not (>= 0 x!16)) (= x!17 0))
         (or (not (>= 0 x!12)) (= x!13 0))
         (or (<= x!13 20) (= x!38 20))
         (or (<= x!17 20) (= x!37 20))
         a!2
         (or (<= 0.0 x!22) (= x!26 0.0))
         (or (and x!31 (not x!28) (not x!29) (>= x!42 15.0) (= x!30 3))
             (= x!9 0))
         (or (and a!4 a!5) a!6)
         (or a!9 (= x!39 6))
         a!12
         (or a!14 (not (<= x!17 20)))
         a!17
         (or a!19 (not (<= 0.0 x!22)))
         (or a!21 (not (<= x!13 20)))
         a!22
         (or a!23 (>= 0 x!15))
         a!24
         (or a!25 (>= 0 x!11))
         a!26
         a!27
         (or (not x!5) (= x!21 x!6))
         (or x!5 (= x!21 x!7))
         (or (not x!27) (= x!8 1))
         (or x!27 (= x!8 0))
         (or x!36 (= x!33 0))
         (or (not x!36) (= x!33 0))
         (or (not x!36) (= x!34 1))
         (or x!36 (= x!34 0))
         (or (not x!36) (= x!39 1))
         (or x!36 (= x!39 0))
         (or x!50 (= x!15 0))
         (or (not x!50) (= x!15 (+ 1 x!1)))
         (or (not x!50) (= x!16 1))
         (or x!50 (= x!16 0))
         (or x!51 (= x!11 0))
         (or (not x!51) (= x!11 (+ 1 x!2)))
         (or (not x!51) (= x!12 1))
         (or x!51 (= x!12 0))
         (= x!35 true)
         (= x!36 true)
         (= x!45 true)
         (= x!48 true)
         (not x!52)
         a!28)))))")
unknown
(:SPACER-expand-node-undef             518
 :SPACER-inductive-level               2
 :SPACER-max-depth                     22
 :SPACER-max-query-lvl                 22
 :SPACER-num-invariants                232
 :SPACER-num-lemmas                    590
 :SPACER-num-properties                9
 :SPACER-num-queries                   597
 :added-eqs                            2273638
 :arith-add-rows                       2001472
 :arith-assert-diseq                   791121
 :arith-assert-lower                   1439682
 :arith-assert-upper                   1547078
 :arith-bound-prop                     15041
 :arith-conflicts                      1281
 :arith-eq-adapter                     357796
 :arith-fixed-eqs                      217304
 :arith-gcd-tests                      779428
 :arith-gomory-cuts                    397
 :arith-ineq-splits                    4458
 :arith-offset-eqs                     172942
 :arith-pivots                         60042
 :bool-inductive-gen                   577
 :bool-inductive-gen-failures          1574
 :conflicts                            8938
 :decisions                            9581259
 :del-clause                           278788
 :final-checks                         18585
 :interface-eqs                        5756
 :max-memory                           69.05
 :memory                               64.46
 :minimized-lits                       16568
 :mk-bool-var                          350640
 :mk-clause                            311478
 :num-allocs                           56854170039.00
 :num-checks                           9593
 :propagations                         14153708
 :restarts                             6
 :rlimit-count                         177141117
 :time                                 82.99
 :time.itp_solver.itp_core             1.46
 :time.spacer.init_rules               0.00
 :time.spacer.init_rules.pt.init       0.00
 :time.spacer.solve                    82.94
 :time.spacer.solve.propagate          36.81
 :time.spacer.solve.reach              46.14
 :time.spacer.solve.reach.children     2.26
 :time.spacer.solve.reach.gen.bool_ind 30.65
 :time.virtual_solver.proof            1.03
 :time.virtual_solver.smt.total        78.74
 :time.virtual_solver.smt.total.sat    35.88
 :time.virtual_solver.smt.total.undef  32.89
 :virtual_solver.checks                9593
 :virtual_solver.checks.sat            1690
 :virtual_solver.checks.undef          2296)
(:max-memory   69.05
 :memory       64.46
 :mk-bool-var  1
 :num-allocs   56854170039.00
 :rlimit-count 177141117
 :time         82.99
 :total-time   83.00)

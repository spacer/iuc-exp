#!/usr/bin/env bash

CPU=930
MEM=8192

IUC=2
IUC_ARITH=2

PROJECT=/ag/iuc-exp
time $PROJECT/bin/z3-nlg -st -v:1 fixedpoint.xform.slice=true fixedpoint.xform.inline_linear=true fixedpoint.xform.inline_eager=true fixedpoint.xform.tail_simplifier_pve=false fixedpoint.engine=spacer fixedpoint.print_statistics=true fixedpoint.spacer.elim_aux=false fixedpoint.spacer.reach_dnf=false fixedpoint.spacer.iuc=$IUC fixedpoint.spacer.iuc.arith=$IUC_ARITH fixedpoint.pdr.validate_result=true  fixedpoint.spacer.ground_cti=false fixedpoint.spacer.mbqi=false fixedpoint.spacer.print_farkas_stats=true -T:$CPU -memory:$MEM $1 

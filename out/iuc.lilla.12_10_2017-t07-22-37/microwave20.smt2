sat
(:SPACER-cex-depth                     4
 :SPACER-expand-node-undef             29
 :SPACER-max-depth                     4
 :SPACER-max-query-lvl                 4
 :SPACER-num-invariants                11
 :SPACER-num-lemmas                    29
 :SPACER-num-properties                24
 :SPACER-num-queries                   29
 :SPACER-num-reach-queries             4
 :SPACER-num-reuse-reach-facts         2
 :added-eqs                            67651
 :arith-add-rows                       95543
 :arith-assert-diseq                   16274
 :arith-assert-lower                   34160
 :arith-assert-upper                   30542
 :arith-bound-prop                     1819
 :arith-conflicts                      231
 :arith-eq-adapter                     12259
 :arith-fixed-eqs                      8127
 :arith-gcd-tests                      25069
 :arith-gomory-cuts                    249
 :arith-ineq-splits                    297
 :arith-offset-eqs                     8993
 :arith-pivots                         6553
 :bool-inductive-gen                   21
 :bool-inductive-gen-failures          29
 :conflicts                            655
 :decisions                            49736
 :del-clause                           17842
 :final-checks                         1178
 :interface-eqs                        440
 :max-memory                           30.12
 :memory                               27.58
 :minimized-lits                       1109
 :mk-bool-var                          31699
 :mk-clause                            31858
 :num-allocs                           174563301
 :num-checks                           223
 :propagations                         281643
 :restarts                             1
 :rlimit-count                         6901523
 :time                                 2.19
 :time.itp_solver.itp_core             0.01
 :time.spacer.init_rules               0.01
 :time.spacer.init_rules.pt.init       0.00
 :time.spacer.solve                    2.11
 :time.spacer.solve.propagate          0.01
 :time.spacer.solve.pt.must_reachable  0.06
 :time.spacer.solve.reach              2.10
 :time.spacer.solve.reach.children     0.03
 :time.spacer.solve.reach.gen.bool_ind 0.18
 :time.spacer.solve.reach.is-reach     0.17
 :time.virtual_solver.proof            0.01
 :time.virtual_solver.smt.total        1.98
 :time.virtual_solver.smt.total.sat    1.44
 :time.virtual_solver.smt.total.undef  0.36
 :virtual_solver.checks                223
 :virtual_solver.checks.sat            49
 :virtual_solver.checks.undef          61)
(:max-memory   30.12
 :memory       27.58
 :mk-bool-var  1
 :num-allocs   174563301
 :rlimit-count 6901523
 :time         2.19
 :total-time   2.20)

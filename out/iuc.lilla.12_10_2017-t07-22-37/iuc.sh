#!/usr/bin/env bash

CPU=930
MEM=8192

PROJECT=/ag/iuc-exp
time $PROJECT/bin/z3-nlg -st -v:1 fixedpoint.xform.slice=false fixedpoint.xform.inline_linear=false fixedpoint.xform.inline_eager=false fixedpoint.xform.tail_simplifier_pve=false fixedpoint.engine=spacer fixedpoint.print_statistics=true fixedpoint.spacer.elim_aux=false fixedpoint.spacer.reach_dnf=false fixedpoint.spacer.new_unsat_core=true fixedpoint.spacer.farkas_a_const=false fixedpoint.spacer.farkas_optimized=false fixedpoint.pdr.validate_result=true fixedpoint.spacer.minimize_unsat_core=false fixedpoint.spacer.ground_cti=false fixedpoint.spacer.mbqi=false -T:$CPU -memory:$MEM $1 
